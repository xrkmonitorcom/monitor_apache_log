#!/bin/bash
NeedCount=1
xrk_plugin=xrk_monitor_apache_log
shdir=`dirname $0`
cd ${shdir}

# check 下是否手动 stop 的
if [ -f _manual_stop_ ] ; then 
	exit;
fi

Count=`pgrep -f xrk_${xrk_plugin} |wc -l`
if [ $Count -lt $NeedCount ] ; then 
	./start.sh > /dev/null 2>&1
fi

