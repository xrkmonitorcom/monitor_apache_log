#!/bin/bash

proc=xrk_monitor_apache_log
Count=`pgrep -f $proc -l |wc -l`
if [ $Count -gt 0 ]; then
    echo "already start"
    exit 0
fi

rm _manual_stop_ > /dev/null 2>&1
if [ ! -x ./install_env.sh ]; then
    echo "start failed, not find file: install_env.sh!"
    exit 1
fi
. ./install_env.sh

if [ ! -f "$APACHE_CFG_FILE" ]; then
    echo "start failed, not find file: $APACHE_CFG_FILE !"
    exit 1
fi

cat "$APACHE_CFG_FILE" |grep "#CustomLog" > /dev/null 2>&1
if [ $? -eq 0 ]; then
    sed -i 's/#CustomLog/CustomLog/' "$APACHE_CFG_FILE"
fi

$APACHE_CMD restart > /dev/null 2>&1
count=0
while [ $count -lt 3 ]
do
    ct=`pgrep -f $proc -l |wc -l`
    if [ $ct -gt 0  ]; then
        echo "start ok, count:$ct"
        exit 0
    fi
    sleep 1
    count=`expr $count + 1`
done

echo "start $proc failed !"
exit 1

