/*** xrkmonitor license ***

   Copyright (c) 2020 by rockdeng

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   字符云监控(xrkmonitor) 开源版 (c) 2019 by rockdeng
   当前版本：v1.0
   使用授权协议： apache license 2.0

   开源版主页：http://open.xrkmonitor.com
   云版本主页：http://xrkmonitor.com
  

   云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
   微信等多种方式，欢迎使用

   外置监控插件 monitor_apache_log 功能:
   		通过 apacache 模块 log_config 监控 apache 网站流量、访问量等指标

****/


#ifndef SLOG_PLUS_APACHE_
#define SLOG_PLUS_APACHE_ 1

#include <map>
#include <list>
#include <iostream>
#include <string.h>

#define MAX_RESPONSE_TIME_INFO_COUNT 15
enum {
	TIME_0_10=0,
	TIME_10_30,
	TIME_30_50,
	TIME_50_100,
	TIME_100_200,
	TIME_200_500,
	TIME_500_1000,
	TIME_1000_2000,
	TIME_be_2000
};

typedef struct _CONFIG
{
	uint32_t dwStartStaticTime; // 一分钟统计项的开始统计时间

	int32_t iCfgId;
	int32_t iShmKey;
	int32_t iLogType;
	int32_t iIpReqCheckTime; // 独立IP 请求量统计时间间隔
	char sLocalLogFile[256];
	char sApacheFmt[512];

	std::list<std::string> listLogItem;
	std::map<std::string, int> mapReqIps; // 来访地域分布
	std::map<std::string, int> mapReqRefers; // 来访链接来源
	std::multimap<int, std::string> mmapDealTimeReq; // 响应耗时排行榜
	std::map<std::string, int> mapReqRes; // 资源访问量分布
	int stRespTimeInfo[MAX_RESPONSE_TIME_INFO_COUNT]; // 耗时分布

	// 用于统计一分钟内的最大响应时间，以及平均响应时间
	int32_t iMaxResponseTime;
	std::string strMaxReqInfo; // 最大响应时的请求链接信息
	std::string strMaxReqIp; // 最大响应时时请求 ip

	uint32_t uiTotalResponseTime;
	int32_t iReqCount;

	_CONFIG() {
		dwStartStaticTime = 0;
		iCfgId = 0;
		iShmKey = 0;
		iLogType = 0;
		iIpReqCheckTime = 60;
		sLocalLogFile[0] = '\0';
		sApacheFmt[0] = '\0';
		memset(&stRespTimeInfo, 0, sizeof(stRespTimeInfo));
		iMaxResponseTime = 0;
		uiTotalResponseTime = 0;
		iReqCount = 0;
	};
}CONFIG;
extern CONFIG stConfig;

#define SLOG_ERROR_LINE -__LINE__

#endif

