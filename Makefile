#
# 手动编译部署插件说明（推荐您使用一键部署功能，一键帮您部署插件到机器）
# 注意：执行手动部署插件前您需要先在监控系统控制台安装插件
#
# 插件编译说明：
# 云版本: 请在首页“下载” 库文件，拷贝到编译机解压后执行 install.sh 初始化编译环境
# 开源版：在码云下载开源版源码，进入源码目录 language/c_cpp  执行 make; make install 初始化编译环境
#
# 编译环境初始化后，下载插件源码解压并执行 make 即可生成插件可执行文件
# 执行 ./make_fabu.sh open 生成开源版部署包
# 执行 ./make_fabu.sh cloud 生成云版部署包
# 将部署包拷贝到机器上解压执行 ./start.sh 即可启动插件, 执行 ./add_crontab.sh 添加crontab 执行监控
#
# 库文件目录为：/usr/lib64, 库文件: libmtreport_api.a(开源版), libmtreport_cloud_api.a(云版)
# 头文件为：/usr/include/mtreport_api(开源版), /usr/include/mtagent_api_lib(云版)
#
# 该 makefile 文件自动识别编译环境，生成对应环境的部署文件
# 如果同时支持开源版和云版本，优先生成开源版, 如需指定生成包可使用对应版本的 makefile 文件，如下：
# make -f Makefile.open(编译开源版), make -f Makefile.cloud(编译云版本)
# 
#
is_open=$(shell if [ -d /usr/include/mtreport_api -a -f /usr/lib64/libmtreport_api.a ]; then echo "1"; else echo "0"; fi;)
is_cloud=$(shell if [ -d /usr/include/mtagent_api_lib -a -f /usr/lib64/libmtreport_cloud_api.a ]; then echo "1"; else echo "0"; fi;)
ifeq ($(is_cloud), 1)
COMM_INC = -I/usr/include/mtagent_api_lib
COMM_LIB = /usr/lib64/libmtreport_cloud_api.a
else ifeq ($(is_open), 1)
COMM_INC = -I/usr/include/mtreport_api 
COMM_LIB = /usr/lib64/libmtreport_api.a
else
$(error please init xrkmonitor environment !)
endif
WARNINGS := -Wall -Wsign-compare -Wno-strict-aliasing -Wno-write-strings -Wno-array-bounds -Wno-unused-but-set-variable -fpermissive -Wno-deprecated-declarations -Wno-literal-suffix
COMM_LIB += -lrt -lpthread

ifeq ($(findstring debug, $(makecmd)), debug)
    WARNINGS += -g 
endif

CXXFLAGS_LIB = $(WARNINGS)  
CXXFLAGS_LINK = 
TARGETS = xrk_monitor_apache_log

CC = g++ 
CXXFLAGS_LIB += $(COMM_INC) -std=c++0x

all:$(TARGETS)

SRC := $(wildcard *.cpp)
OBJ := $(SRC:.cpp=.o)

.cpp.o:
	$(CC) $(CXXFLAGS_LIB) -c $(filter %.cpp, $^)

$(TARGETS): ${OBJ} 
	$(CC) $(CXXFLAGS_LIB) $(CXXFLAGS_LINK) -o $@ $^  $(COMM_LIB)
ifneq ($(findstring debug, $(makecmd)), debug)
	strip $(TARGETS)
endif

clean:
	rm -f *.o
	rm -f $(TARGETS) 

