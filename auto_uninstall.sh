#/bin/bash
PATH=/bin:/sbin:/usr/bin/:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/mysql/bin
# ---------------------------------------------------------------------------------
# 字符云监控(xrkmonitor) 开源版 (c) 2020 by rockdeng
# 使用授权协议： apache license 2.0
#
# 开源演示版网址: http://open.xrkmonitor.com
# 云版本主页：http://xrkmonitor.com
#
# 云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
# 微信等多种方式，欢迎使用
# ---------------------------------------------------------------------------------
#
# web 后端插件: monitor_apache_log 一键卸载脚本
#

function AddInstallLog()
{
    if [ -f "$install_log_file" ]; then
        logt=`date '+%Y-%m-%d %H:%M:%S.%N'`
        echo "[ $logt ] shell - $1 " >> $install_log_file
    else
        echo $1
    fi
}

if [ ! -x ./install_env.sh -o ! -x ./stop.sh ]; then
    AddInstallLog "xrk_failed, not find file:install_env.sh or stop.sh !"
    exit 1
fi

. ./install_env.sh
./stop.sh

# 依赖模块 log_config
$APACHE_CMD -t -D DUMP_MODULES  |grep log_config_module > /dev/null 2>&1
if [ $? -ne 0 ]; then
    AddInstallLog "not find apache module: log_config"
    echo "xrk_failed 0 0"
    exit 1
fi

xrk_success=0
xrk_failed=0

#修改 apache 默认配置文件，加入插件的 apache 配置文件 
incFile="apache_monitor_apache_log.conf"
cat "$APH_SERVER_CONFIG_FILE" |grep "$incFile" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    AddInstallLog "already remove |$incFile| from file: $APH_SERVER_CONFIG_FILE"
    xrk_success=`expr $xrk_success + 1`
else
    sed -i "/$incFile/d" "$APH_SERVER_CONFIG_FILE"
    cat "$APH_SERVER_CONFIG_FILE" |grep "$incFile" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        AddInstallLog "[ change ] - remove $incFile from file: $APH_SERVER_CONFIG_FILE ok"
        xrk_success=`expr $xrk_success + 1`
    else
        AddInstallLog "remove $incFile from file: $APH_SERVER_CONFIG_FILE failed !"
        xrk_failed=`expr $xrk_failed + 1`
    fi
fi

# 各虚拟主机处理, 引入插件配置文件
sAphConfList=`$APACHE_CMD -t -D DUMP_INCLUDES|awk '{if(NR!=1)print $2}'|sort -u`
if [ "$sAphConfList" == '' ]; then
    sAphConfList=`find "$APH_SERVER_CONFIG_PATH" -name "*.conf"`
fi
sVHostFileList=`grep -v "[[:space:]]*#" $sAphConfList|grep "<VirtualHost" |awk -F ":" '{ print $1}'|sort -u`
for sVF in $sVHostFileList
do
    cat "$sVF" |grep "$incFile" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        AddInstallLog "already remove |$incFile| from file: $sVF"
    else
        sed -i "/$incFile/d" "$sVF"
        cat "$sVF" |grep "$incFile" > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            AddInstallLog "[ change ] - remove $incFile from file: $sVF ok"
            xrk_success=`expr $xrk_success + 1`
        else
            AddInstallLog "remove $incFile from file: $sVF failed !"
            xrk_failed=`expr $xrk_failed + 1`
        fi
    fi
done

# 这个文件的移除要放后面，否则 apachectl 会报错，因为有其它配置文件引用它
if [ -f "$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf" ]; then
    rm -f "$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf" > /dev/null 2>&1
    if [ -f "$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf" ]; then
        AddInstallLog "failed to remove apache config file:$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf"
    else
        AddInstallLog "[ change ] - remove apache config file:$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf ok"
    fi
else
    AddInstallLog "not find apache config file:$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf"
fi

if [ $xrk_failed -eq 0 ]; then 
    echo "xrk_success $xrk_success $xrk_failed";
elif [ $xrk_success -gt 0 ]; then
    echo "xrk_psuccess $xrk_success $xrk_failed";
else
    echo "xrk_failed $xrk_success $xrk_failed";
fi

$APACHE_CMD restart > /dev/null 2>&1

