# apache 网站监控插件： monitor_apache_log

#### 插件介绍

![](http://xrkmonitor.com/monitor/download/plugin/ppic_5n3nuutk_apache_plus.png)

通过 apache 的日志模块：log_config 监控 apache 站点，监控数据包括网站流量，网站访问数据等。  
实现方法：通过 apache log_config 模块的 CustomLog指令将站点访问日志通过管道导入到插件   
monitor_apache_log 中，插件通过分析日志得到站点相关数据然后上报的监控系统中。  

插件演示：http://open.xrkmonitor.com/, 进入 “监控点上报查看”-》“视图上报查看” 菜单   
xrkmonitor 字符云监控系统源码链接：https://gitee.com/xrkmonitorcom/open   

监控控制台支持一键部署、启用、禁用、移除等操作。编译方法见 Makefile 说明

#### 插件监控界面数据展示    

部分监控点数据图表：
![](http://xrkmonitor.com/monitor/images/monitor_apache_log2.png)
![](http://xrkmonitor.com/monitor/images/monitor_apache_log1.png)

日志数据展示：
![](http://xrkmonitor.com/monitor/images/monitor_apache_log.png)


