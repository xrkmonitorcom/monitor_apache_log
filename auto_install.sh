#!/bin/bash
# ---------------------------------------------------------------------------------
# 字符云监控(xrkmonitor) 开源版 (c) 2020 by rockdeng
# 使用授权协议： apache license 2.0
#
# 开源演示版网址: http://open.xrkmonitor.com
# 云版本主页：http://xrkmonitor.com
#
# 云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
# 微信等多种方式，欢迎使用
# ---------------------------------------------------------------------------------
#
# web 后端插件: monitor_apache_log 一键部署脚本
#

plugin_install_dir=`pwd`

function AddInstallLog()
{
    if [ -f "$install_log_file" ]; then
        logt=`date '+%Y-%m-%d %H:%M:%S.%N'`
        echo "[ $logt ] shell - $1 " >> $install_log_file
    else 
        echo $1
    fi
}

function GetApacheCfgPath()
{
    sAphServerCfgFile=`$APACHE_CMD -V|grep SERVER_CONFIG_FILE|awk -F "=" '{print $2}'`
    APH_SERVER_CONFIG_FILE=${sAphServerCfgFile//\"/}
    if [ ! -f "$APH_SERVER_CONFIG_FILE" ]; then
        ap_sp=`expr substr $APH_SERVER_CONFIG_FILE 1 1`
        if [ "$ap_sp" != "/" ]; then
            sAphRoot=`$APACHE_CMD -V|grep HTTPD_ROOT|awk -F "=" '{print $2}'`
            sAphRoot=${sAphRoot//\"/}
            APH_SERVER_CONFIG_FILE=${sAphRoot}/$APH_SERVER_CONFIG_FILE
            if [ ! -f "$APH_SERVER_CONFIG_FILE" ]; then
                echo "xrk_failed not find apache config file"
                exit 1
            fi
        else
            echo "xrk_failed not find apache config file"
            exit 1
        fi
    fi
    AddInstallLog "get apache config file: $APH_SERVER_CONFIG_FILE"

    APH_SERVER_CONFIG_PATH=`dirname "$APH_SERVER_CONFIG_FILE"`
    if [ ! -d "$APH_SERVER_CONFIG_PATH" ]; then
        echo "xrk_failed not find apache config path!"
        exit 1
    fi
    AddInstallLog "get apache config path: $APH_SERVER_CONFIG_PATH"
}

function GetApacheCmd()
{
    apachectl -v > /dev/null 2>&1 
    if [ $? -eq 0 ]; then 
        APACHE_CMD=apachectl
    else
        apache2ctl -v > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            APACHE_CMD=apache2ctl
        else
            echo "xrk_failed not find apache cmd !"
            exit 1
        fi
    fi
    AddInstallLog "find apache exe cmd: $APACHE_CMD"
}

GetApacheCmd
GetApacheCfgPath

# 依赖模块 log_config
$APACHE_CMD -t -D DUMP_MODULES  |grep log_config_module > /dev/null 2>&1
if [ $? -ne 0 ]; then
    AddInstallLog "not find apache module: log_config"
    echo "xrk_failed 0 0"
    exit 1
fi

# 生成 apache 插件部署配置文件到 apache 配置目录
sed "s#PLUGIN_INSTALL_DIR#$plugin_install_dir#g" _apache_monitor_apache_log.conf > "$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf"
if [ -f "$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf" ]; then
    AddInstallLog "[ change ] - create apache config file:$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf"
else
    AddInstallLog "create apache config file:$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf failed!"
    exit 1
fi

xrk_success=0
xrk_failed=0

#修改 apache 默认配置文件，加入插件的 apache 配置文件 
incFile="Include $APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf"
cat "$APH_SERVER_CONFIG_FILE" |grep "$incFile" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "$incFile" >> "$APH_SERVER_CONFIG_FILE"
    cat "$APH_SERVER_CONFIG_FILE" |grep "$incFile" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        AddInstallLog "[ change ] - add $incFile to file: $APH_SERVER_CONFIG_FILE ok"
        xrk_success=`expr $xrk_success + 1`
    else
        AddInstallLog "add $incFile in file: $APH_SERVER_CONFIG_FILE failed !"
        xrk_failed=`expr $xrk_failed + 1`
    fi
else
    AddInstallLog "already add |$incFile| in file: $APH_SERVER_CONFIG_FILE"
fi

# 各虚拟主机处理, 引入插件配置文件
sAphConfList=`$APACHE_CMD -t -D DUMP_INCLUDES|awk '{if(NR!=1)print $2}'|sort -u`
if [ "$sAphConfList" == '' ]; then
    sAphConfList=`find "$APH_SERVER_CONFIG_PATH" -name "*.conf"`
fi
sVHostFileList=`grep -v "[[:space:]]*#" $sAphConfList|grep "<VirtualHost" |awk -F ":" '{ print $1}'|sort -u`
for sVF in $sVHostFileList
do
    cat "$sVF" |grep "$incFile" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        sed -i "/<VirtualHost/a$incFile" "$sVF"
        cat "$sVF" |grep "$incFile" > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            AddInstallLog " [ change ] - add $incFile in file: $sVF ok"
            xrk_success=`expr $xrk_success + 1`
        else
            AddInstallLog "add $incFile in file: $sVF failed"
            xrk_failed=`expr $xrk_failed + 1`
            continue
        fi
    else
        AddInstallLog "already add |$incFile| in file: $sVF"
    fi
done

if [ $xrk_failed -eq 0 ]; then 
    echo "xrk_success $xrk_success $xrk_failed";
elif [ $xrk_success -gt 0 ]; then
    echo "xrk_psuccess $xrk_success $xrk_failed";
else
    echo "xrk_failed $xrk_success $xrk_failed";
fi

echo "#!/bin/bash" > install_env.sh
echo "APACHE_CMD=$APACHE_CMD" >> install_env.sh
echo "APACHE_CFG_FILE=\"$APH_SERVER_CONFIG_PATH/apache_monitor_apache_log.conf\"" >> install_env.sh
echo "APH_SERVER_CONFIG_FILE=\"$APH_SERVER_CONFIG_FILE\"" >> install_env.sh
echo "APH_SERVER_CONFIG_PATH=\"$APH_SERVER_CONFIG_PATH\"" >> install_env.sh

chmod +x install_env.sh > /dev/null 2>&1
./start.sh 

