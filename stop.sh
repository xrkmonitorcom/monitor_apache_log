#!/bin/bash

proc=xrk_monitor_apache_log
Count=`pgrep -f $proc -l |wc -l`
if [ $Count -eq 0 ]; then
    echo "already stop"
    exit 0
fi

if [ ! -x ./install_env.sh ]; then
    echo "stop failed, not find file: install_env.sh!"
    exit 1
fi
. ./install_env.sh

if [ ! -f "$APACHE_CFG_FILE" ]; then
    echo "stop failed, not find file: $APACHE_CFG_FILE !"
    exit 1
fi

dpat="#CustomLog"
cat "$APACHE_CFG_FILE" |grep "$dpat" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    sed -i "s/CustomLog/$dpat/" "$APACHE_CFG_FILE"
    cat "$APACHE_CFG_FILE" |grep "$dpat" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "stop failed, add $dpat failed !"
        exit 1
    fi
fi

$APACHE_CMD restart > /dev/null 2>&1
count=0
while [ $count -lt 5 ]
do
    ct=`pgrep -f $proc -l |wc -l`
    if [ $ct -eq 0  ]; then
        echo "stop ok"
        exit 0
    fi
    sleep 1
    count=`expr $count + 1`
done

echo "stop failed !"
exit 1

