/*********************************************************************

	该文件为字符云监控系统插件源码头文件, 跟插件版本相关
	请勿在该文件中添加您的私有代码, 以免版本升级时影响代码合并

	插件显示名: 基于apache 日志的网站服务器端监控
	插件部署名: monitor_apache_log
	插件ID: 16
	文件版本: v1.0.5
	使用授权协议  Apache-2.0

 ***********************************************************************/

#ifndef _XRKMONITOR_PLUGIN_16_H_
#define _XRKMONITOR_PLUGIN_16_H_ 1 

#include <sv_cfg.h>
#include <mt_log.h>
#include <sv_coredump.h>

#define XRK_PLUGIN_HEADER_FILE_VER "v1.0.5" 
#define XRK_PLUGIN_ID 16 
#define XRK_PLUGIN_NAME "monitor_apache_log"
#define CORE_INFO_FILE "./core_monitor_apache_log.log" 
extern MtReport g_mtReport;
extern int g_aryPluginAttr[10]; 
#define XRK_ACC_COUNT g_aryPluginAttr[0]
#define XRK_ACC_IP_COUNT g_aryPluginAttr[1]
#define XRK_ACC_MAX_RESP_TIME g_aryPluginAttr[2]
#define XRK_ACC_AVG_RESP_TIME g_aryPluginAttr[3]
#define XRK_ACC_REGION_DIS g_aryPluginAttr[4]
#define XRK_ACC_LINK_FROM g_aryPluginAttr[5]
#define XRK_SITE_OUT_DATA g_aryPluginAttr[6]
#define XRK_SITE_ACC_TOTAL g_aryPluginAttr[7]
#define XRK_RESOURCE_ACC_DIS g_aryPluginAttr[8]
#define XRK_RESP_TIME_DIS g_aryPluginAttr[9]
#define XRK_PLUGIN_ATTRS_COUNT_MAX 10 
#define XRK_PLUGIN_ALL_ATTRS "XRK_ACC_COUNT XRK_ACC_IP_COUNT XRK_ACC_MAX_RESP_TIME XRK_ACC_AVG_RESP_TIME XRK_ACC_REGION_DIS XRK_ACC_LINK_FROM XRK_SITE_OUT_DATA XRK_SITE_ACC_TOTAL XRK_RESOURCE_ACC_DIS XRK_RESP_TIME_DIS "

#endif

