#!/bin/bash
PLUGIN=monitor_apache_log
TARGET=${PLUGIN}.tar.gz

if [ $# -ne 1 ];then
	echo "use ./make_fabu.sh open|cloud"
	exit 1
fi

COMM_FILE="auto_install.sh auto_uninstall.sh restart.sh start.sh stop.sh add_crontab.sh remove_crontab.sh cron_${PLUGIN}.sh xrk_${PLUGIN} _apache_monitor_apache_log.conf "

rm -f $TARGET > /dev/null 2>&1
if [ "$1" == "cloud" ]; then
	tar -czf ${TARGET} $COMM_FILE xrk_monitor_apache_log.conf 
elif [ "$1" == "open" ]; then
	tar -czf ${TARGET} $COMM_FILE 
else
	echo "use ./make_fabu.sh open|cloud"
fi

if [ -d /srv/www/monitor/download/packet/linux ]; then
	echo "cp ${TARGET} /srv/www/monitor/download/packet/linux"
	cp ${TARGET} /srv/www/monitor/download/packet/linux
elif [ -d /home/monitor_cms/public/monitor/download/packet/linux ]; then
    echo "cp ${TARGET} /home/monitor_cms/public/monitor/download/packet/linux"
    cp ${TARGET} /home/monitor_cms/public/monitor/download/packet/linux
fi

